import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * This class will take a properly parsed input file and create the GUI.
 * We have created several internal classes to aid in the GUI creation. These class have methods
 * that call components of the Java Swing class
 */
public class ComponentsBuilder {



    public JFrame buildComponents(File inputFile) throws FileNotFoundException {

        // Create a new scanner object that reads in input
        Scanner sc = new Scanner(new BufferedReader(new FileReader(inputFile)));

        // Window window;
        Window window = new Window();

        // By default, the current container is a window
        Container currentContainer = window;

        // While file has values, add them to the dynamic array
        while (sc.hasNext()) {

            switch (sc.next()) {
                // It is a WINDOW? Build a window
                case GUITokens.WINDOW_KEYWORD:
                    // Set the next token as a temporary string
                    String temporaryString = sc.next();
                    // The name of the window is the substring which excludes the quotes
                    String windowName = temporaryString.substring(1, temporaryString.length() - 1);
                    // Initialize the size of the window
                    int width = 0;
                    int height = 0;

                    // If we have integers within the parenthesis
                    while (sc.hasNextInt() && !(sc.next().equals(GUITokens.R_PARENS))) {
                        // the width is the first integer
                        width = sc.nextInt();
                        // the height is the second integer
                        height = sc.nextInt();
                    }
                    window = new Window(windowName, height, width);
                    break;

                // If we run across a layout keyword
                case GUITokens.LAYOUT_KEYWORD:
                    // The layout type will be the next string
                    String layoutType = sc.next();

                    // Check what kind of layout it is
                    switch (layoutType) {

                        // If it is a Grid
                        case GUITokens.GRID_KEYWORD:
                            // Declare a new grid
                            Grid grid;

                            // Initialize the default rows
                            int rows = 1;
                            // Initialize the default columns
                            int columns = 1;

                            // Initialize default v and h gaps
                            int vgap = 0;
                            int hgap = 0;

                            // While we have integers and we have not reached a right parenthesis token
                            while (sc.hasNextInt() && !(sc.next().equals(GUITokens.R_PARENS))) {

                                // the rows equal the first integer
                                rows = sc.nextInt();
                                // the columns equal the second integer
                                columns = sc.nextInt();
                                // if there is a third integer
                                vgap = sc.nextInt();
                                // if there is a fourth integer
                                hgap = sc.nextInt();
                            }
                            // If vgap and hgap have been assigned a value by the user which is
                            // greater than zero
                            if ((vgap > 0) && (hgap > 0)) {
                                grid = new Grid(rows, columns, vgap, hgap);
                            } else {
                                grid = new Grid(rows, columns);
                            }
                            // Set the layout of the current container to grid, and build the layout
                            // using Java swing components
                            currentContainer.setLayout(grid.build());
                            // Break
                            break;
                        // If it is not a grid keyword, or any keyword, then it is the default
                        // flow layout
                        default:
                            // Get the current container
                            // and set the layout of the panel to
                            // flow
                            Flow flow = new Flow();
                            // Set the layout of the current container to flow, and build the layout
                            // using Java swing components
                            currentContainer.setLayout(flow.build());
                            //break
                            break;

                        // Is it a BUTTON?
                        case GUITokens.BUTTON_KEYWORD:
                            // Set a temporary string variable to the next token, which should be
                            // "7"
                            String tempString = sc.next();
                            // Get the name of that button by getting the substring, excluding the
                            // quotes on either end
                            String name = tempString.substring(1, tempString.length() - 1);
                            // Initialize a new internal button object
                            Button button = new Button(name);
                            // add this button to the current container, and build the button using
                            // Java swing components
                            currentContainer.add(button.build());
                            break;

                        // Is it a TEXTFIELD?
                        case GUITokens.TEXTFIELD_KEYWORD:
                            // Initialize the number of columns to 0
                            int textFieldColumns = 0;

                            // If the next token is an integer
                            if (sc.hasNextInt()) {
                                // set the next int as
                                textFieldColumns = sc.nextInt();
                            }
                            // Create a new internal text field object, initializing with the
                            // desired columns
                            TextField textField = new TextField(textFieldColumns);
                            // Add the new text field to the current container, and build the
                            // text field using Java swing components
                            currentContainer.add(textField.build());
                            break;

                        // Is it a LABEL?
                        case GUITokens.LABEL_KEYWORD:
                            //If we have a label keyword, the next token "labelname" will be
                            // surrounded in quotes, set the next token to a local string variable
                            String rawLabelName = sc.next();
                            // The true name of the label is the substring of the raw string,
                            // excluding the quotes
                            String trueLabelName =
                                rawLabelName.substring(1, rawLabelName.length() - 1);
                            // Initialize a new internal label object with the true label name
                            Label label = new Label(trueLabelName);
                            // Add the label to the current container, and build the label using
                            // Java swing components
                            currentContainer.add(label.build());
                            break;

                        // Is it a PANEL? Build the panel
                        case GUITokens.PANEL_KEYWORD:
                            // Create a new internal panel object
                            Panel panel = new Panel();
                            // Add the panel to the current container
                            currentContainer.add(panel.build());
                            // Set the current container to the new panel
                            currentContainer = panel;
                            break;
                        // Is it PERIOD? We have reached the end of the program, so BREAK
                        case GUITokens.PERIOD:
                            // Marks the end of the input file, so break and proceed to build GUI
                    }
            }
        }
        // Close the scanner to save resources
        sc.close();
        // Return the new JFrame
        return window.build();
    }



    /**
     * WINDOW CLASS
     */
    class Window extends Container {
        // Components of a window
        String name;
        int height;
        int width;
        Layout layout = new Flow();

        /**
         * Window class constructor
         *
         * @param name
         * @param height
         * @param width
         */
        public Window(String name, int height, int width) {
            this.name = name;
            this.height = height;
            this.width = width;
        }

        public Window() {
        }

        public JFrame build() {
            JFrame self = new JFrame();
            self.setSize(width, height);
            self.setName(name);
            self.setVisible(true);
            self.setLayout(layout.build());
            return self;
        }
    }


    /**
     * PANEL CLASS
     */
    class Panel extends Container {
        // Components of a panel
        Layout layout;

        /**
         * Panel class constructor
         *
         * @param layout
         */
        public Panel(Layout layout) {
            this.layout = layout;
        }

        public Panel() {
        }


        public JPanel build() {
            JPanel jPanel = new JPanel();
            jPanel.setLayout(layout.build());

            return jPanel;
        }
    }


    /**
     * LAYOUT PARENT CLASS
     */
    abstract class Layout {

        abstract LayoutManager build();
    }


    /**
     * GRID LAYOUT CLASS
     */
    class Grid extends Layout {
        int rows;
        int columns;
        int vgap = 0;
        int hgap = 0;

        public Grid(int rows, int columns, int vgap, int hgap) {
            this.rows = rows;
            this.columns = columns;
            this.vgap = vgap;
            this.hgap = hgap;
        }

        public Grid(int rows, int columns) {
            this.rows = rows;
            this.columns = columns;
        }


        @Override LayoutManager build() {

            LayoutManager gridLayout;

            if ((vgap > 0) || (hgap > 0)) {
                gridLayout = new GridLayout(rows, columns, vgap, hgap);
            } else {
                gridLayout = new GridLayout(rows, columns);
            }
            return gridLayout;
        }
    }


    /**
     * FLOW LAYOUT CLASS
     */
    class Flow extends Layout {

        @Override LayoutManager build() {
            LayoutManager flowLayout = new FlowLayout();
            return flowLayout;
        }
    }


    /**
     * WIDGET PARENT CLASS
     */
    interface Widget {


        Component build();
    }


    /**
     * TEXTFIELD CLASS
     * Create a text field widget
     */
    class TextField implements Widget {
        private static final String type = "TextField";
        Integer numberOfColumns;

        public TextField(Integer numberOfColumns) {
            this.numberOfColumns = numberOfColumns;
        }

        @Override public String toString() {
            return type;
        }

        @Override public JTextField build() {
            JTextField textField = new JTextField();
            textField.setColumns(numberOfColumns);

            return textField;
        }
    }


    /**
     * LABEL CLASS
     * Create a label widget
     */
    class Label implements Widget {
        private static final String type = "Label";
        String labelName;

        public Label(String labelName) {
            this.labelName = labelName;
        }

        @Override public String toString() {
            return type;
        }

        @Override public JLabel build() {
            JLabel label = new JLabel();
            label.setName(labelName);
            return label;
        }
    }


    /**
     * BUTTON CLASS
     * Create a button widget
     */
    class Button implements Widget {
        private static final String type = "Button";
        String buttonName;

        public Button(String buttonName) {
            this.buttonName = buttonName;
        }

        @Override public String toString() {
            return type;
        }

        @Override public JButton build() {
            JButton button = new JButton();
            button.setName(buttonName);
            return button;
        }
    }
}


