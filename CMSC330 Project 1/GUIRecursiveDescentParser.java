import java.io.*;
import java.util.*;


/**
 * File: GUIRecursiveDescentParser.java
 * Author: Yousuf Nejati
 * Date: November 19th 2015
 *
 * Purpose: Write a  program that parses, using recursive descent, a GUI definition language defined
 * in an input file and generates the GUI that it defines with the project assigned grammar.
 * The parser properly handles the fact that panels can be nested in other panels. Recursive productions
 * are implemented using recursion. Syntactically incorrect
 *
 * RDP- use the current input symbol to decide which alternative to choose
 * Called LL(1)
 *
 * INPUT: Tokens developed by the Lexer or tokenizer are passed to the parser
 * Parser analyzes syntax error and for translating an error free program into internal data strucutures
 * that can be interpreted by another language
 *
 * GET THE TOKENS AND CHECK THEM AGAINST THE LANGUAGE AGAINST THE GRAMMAR
 * IF THE GRAMMAR IS CORRECT PASS IT TO SWING
 */
public class GUIRecursiveDescentParser {

    // In order to maintain LL(1) we use the peek method to get next token
    // This field is initialized in the constructor
    private Queue<String> tokenStream;
    static String currentToken;
    static String nextToken;


    // Integers keep track of character pairs, each opening character we increment 1, each closing
    // character we decrement 1. If > or < 0 then we throw an character match exception
    private int parenMatch;


    /**
     * Constructor, accepts an input token stream from scanner, this Queue is used as the
     * token stream for our parser
     */
    public GUIRecursiveDescentParser(Queue<String> tokenStream) {
        this.tokenStream = tokenStream;

    }

    /**
     * GUI subroutine
     *
     * Calls on several subroutines (productions) , while to token stream is not empty we check each token
     * in the token stream (as a string) against the tokens defined by the provided grammar
     * If at any point the syntax does not mach the semantics of the grammar we throw a parsing
     * error and return false
     *
     */
    public boolean guiParser() throws ParseError {

        boolean expected = false;

        // For the length of the token stream (in this case 64), remove the current token from the
        // queue, setting it to the currentToken, and peek the next token setting it to nextToken
        // Run through the main routine below, calling recursive methods on subroutines

        while (!(tokenStream.isEmpty())) {

            // Get the current tonken by removing it from the top of the queue
            currentToken = tokenStream.remove();
            // And, we peak at the next token in the queue
            nextToken = tokenStream.peek();

            try {
                // Get the current token
                switch (currentToken) {

                    // If the current token is "Window"
                    case GUITokens.WINDOW_KEYWORD:
                        // Then we expect the next token to be a string
                        expected = (GUITokens.isStringToken(nextToken));
                        break;
                    case GUITokens.L_PARENS:
                        expected = (GUITokens.isNumberToken(nextToken));
                        parenMatch++;
                        break;
                    case GUITokens.COMMA:
                        expected = (GUITokens.isNumberToken(nextToken));
                        break;
                    case GUITokens.R_PARENS:
                        // call the layout subroutine, starting with the next token
                        // If the layout subroutine parses correctly, call the widgets subroutine
                        parenMatch--;
                        if (layout()) {
                            if (widgets()) {
                                if (nextToken == null||nextToken.equals(GUITokens.END)) {
                                    return true;
                                }
                            }
                            else return false;
                        }

                        break;
                    case GUITokens.END:
                        expected = nextToken.equals(GUITokens.PERIOD);
                        break;
                    case GUITokens.PERIOD:
                        expected = true;
                        break;
                }

                if (GUITokens.isStringToken(currentToken)) {
                    // Yes, then the next expected token must be an open parenthesis....
                    expected = nextToken.equals(GUITokens.L_PARENS);
                }
                if (GUITokens.isNumberToken(currentToken)) {
                    //Yes, then the next expected token must be a comma or a close parenthesis......
                    expected =
                        nextToken.equals(GUITokens.COMMA) || nextToken.equals(GUITokens.R_PARENS);
                }
            } catch (NumberFormatException e) {
                //   return expected;
                System.out.println("Exception occurred");
                e.printStackTrace();
            }
        }
        // Parenthesis match check
        if (parenMatch != 0) {
            throw new ParenthesisMatchException("Parenthesis don't match up");
        }
        // Return whether the expected value matches production
        return expected;
    }


    /**
     * Layout subroutine ++++++++++++OK
     */
    private boolean layout() throws ParseError {
        // Every time we start a new subroutine, we remove a new current token from the queue
        currentToken = tokenStream.remove();
        // And, we peak at the next token in the queue
        nextToken = tokenStream.peek();

        boolean expected = false;

        switch (currentToken) {

            case GUITokens.LAYOUT_KEYWORD:
                // Call the layout subroutine
                // If the layout type parses correctly, then check the next token
                if (layout_type()) {
                    if (nextToken.equals(GUITokens.COLON)) {
                        // Every time we start a new subroutine, we remove a new current token from the queue
                        currentToken = tokenStream.remove();
                        // And, we peak at the next token in the queue
                        nextToken = tokenStream.peek();
                        return true;
                    }
                } else
                    return false;

                break;
            case GUITokens.COLON:
                expected = true;
                break;
        }

        return expected;
    }

    /**
     * Layout_type subroutine+++++++++++++OK
     */
    private boolean layout_type() throws ParseError {
        // Every time we start a new subroutine, we remove a new current token from the queue
        currentToken = tokenStream.remove();
        // And, we peak at the next token in the queue
        nextToken = tokenStream.peek();
        boolean expected = true;


        switch (currentToken) {
            case GUITokens.FLOW_KEYWORD:
                expected = nextToken.equals(GUITokens.COLON);
                break;
            case GUITokens.GRID_KEYWORD:

                if(!nextToken.equals(GUITokens.L_PARENS)) return false;
                // Every time we start a new subroutine, we remove a new current token from the queue
                currentToken = tokenStream.remove();
                currentToken = tokenStream.remove();
                if(!GUITokens.isNumberToken(currentToken)) return false;

                currentToken =tokenStream.remove();
                if(!currentToken.equals(GUITokens.COMMA)) return false;
                currentToken =tokenStream.remove();
                if(!GUITokens.isNumberToken(currentToken)) return false;

                // And, we peak at the next token in the queue
                nextToken = tokenStream.peek();
                // This is the optional third and fourth numbers
                if (nextToken.equals(GUITokens.COMMA)) {
                    currentToken =tokenStream.remove();
                    currentToken =tokenStream.remove();
                    if(!GUITokens.isNumberToken(currentToken)) return false;
                    currentToken =tokenStream.remove();
                    if(!currentToken.equals(GUITokens.COMMA)) return false;
                    currentToken =tokenStream.remove();
                    if(!GUITokens.isNumberToken(currentToken)) return false;
                }

                currentToken = tokenStream.remove();
                if(!currentToken.equals(GUITokens.R_PARENS)) return false;
                nextToken = tokenStream.peek();
                return true;

        }

        return expected;
    }

    /**
     * Widgets subroutine +++++++++++ OK
     */
    private boolean widgets() throws ParseError {
        // Create a local boolean
        boolean expected;

        expected = widget() && widgets() || widget();

        return expected;
    }

    /**
     * Widget subroutine ++++++++++++ OK
     */
    private boolean widget() throws ParseError {
        // Every time we start a new subroutine, we remove a new current token from the queue
        currentToken = tokenStream.remove();
        // And, we peak at the next token in the queue
        nextToken = tokenStream.peek();
        boolean expected = false;

        switch (currentToken) {

            case GUITokens.BUTTON_KEYWORD:
                expected = (GUITokens.isStringToken(nextToken));

                break;
            case GUITokens.GROUP_KEYWORD:
                expected = radioButtons();

                break;
            case GUITokens.SEMICOLON:
                expected = true;
                break;
            case GUITokens.END:
                expected = nextToken.equals(GUITokens.SEMICOLON);

                break;
            case GUITokens.LABEL_KEYWORD:
                expected = (GUITokens.isStringToken(nextToken));

                break;
            case GUITokens.PANEL_KEYWORD:
                expected = false;
                if (layout()) {
                    if (widgets()) {
                        return true;
                    }
                    return true;
                }
                break;
            case GUITokens.TEXTFIELD_KEYWORD:
                expected = (GUITokens.isNumberToken(nextToken));
                break;
            case GUITokens.PERIOD:
                return true;
        }

        if (GUITokens.isStringToken(currentToken)) {
            // Yes, then the next expected token must be an
            expected = nextToken.equals(GUITokens.SEMICOLON);
        }

        // Number check
        if (GUITokens.isNumberToken(nextToken)) {
            //Then the token is a Number
            // Every time we start a new subroutine, we remove a new current token from the queue
            currentToken = tokenStream.remove();
            // And, we peak at the next token in the queue
            nextToken = tokenStream.peek();
            expected = nextToken.equals(GUITokens.SEMICOLON);
        }

        return expected;
    }

    /**
     * radioButtons subroutine +++++++++++++++++
     */
    private boolean radioButtons() {
        boolean expected;
        expected = radioButton() && radioButtons() || radioButton();
        return expected;
    }

    /**
     * radioButton subroutine +++++++++++++++++++
     */
    private boolean radioButton() {
        // Every time we start a new subroutine, we remove a new current token from the queue
        currentToken = tokenStream.remove();
        // And, we peak at the next token in the queue
        nextToken = tokenStream.peek();
        boolean expected = true;

        switch (currentToken) {
            // Is the current token a radio keyword
            case GUITokens.RADIO_KEYWORD:
                // we expect the next token to be a string
                expected = (GUITokens.isStringToken(nextToken));
                // If false, return false
                if (!expected)
                    return false;
                break;

            case GUITokens.SEMICOLON:
                expected = true;
        }
        // Is the current token a string
        if (GUITokens.isStringToken(currentToken)) {
            // Yes, then the next expected token must be an
            expected = nextToken.equals(GUITokens.SEMICOLON);
        }

        return expected;
    }


    /**
     * DRIVER
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException, ParseError {

        // Create a new file to read from, directory is hard coded!
        File inputFile =
            new File("/Users/yousufnejati/IdeaProjects/CMSC330/InputFiles/CalculatorInput");
        // Tokenize this new input file by passing into the Tokenizer constructor
        GUITokenizer guiTokenizer = new GUITokenizer(inputFile);
        // Set the tokenized stream to a local Queue
        Queue<String> tokenStream = guiTokenizer.tokenStream();

        // Initialize the guiRDP with the local queue
        GUIRecursiveDescentParser parserRD = new GUIRecursiveDescentParser(tokenStream);

        // Call the guiParser method from the parser and assign it to a local boolean value
        // If the guiRDP evaluates to true, the input file has parsed correctly
        boolean doesParse = parserRD.guiParser();

        // If the file can be parsed using the Recursive Descent Parser, build the guiParser
        if (doesParse) {
            System.out.println("true"); // <-- Prints to system for temporary trouble shooting

            /*

            // Build the components from the file
            ComponentsBuilder constComponents = new ComponentsBuilder();

            // Build the guiParser from the components
            constComponents.buildComponents(inputFile);

            */
        }
        // If not, throw a parsing exception
        else {
            System.out.println("False"); // <-- Prints to system for temporary trouble shooting
            throw new ParseError("Does not parse correctly");
        }

    }
}


