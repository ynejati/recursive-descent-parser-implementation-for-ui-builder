import com.sun.tools.javac.parser.Tokens;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by yousufnejati on 11/11/15.
 */
public class GUITokenizer {


    private File inputFile;

    /**
     *
     * @param inputFile
     */
    public GUITokenizer(File inputFile) {
        this.inputFile = inputFile;
    }

    /**
     * Method acts as the lexer taking in the parameter provided by the creation of
     * a new class object
     *
     * ASSUME: all significant punctuation, keywords, numbers, etc are separated by
     * white space in order for the tokenizer to recognize significant tokens
     *
     * @return Queue of tokens
     * @throws FileNotFoundException
     */
    public Queue<String> tokenStream() throws FileNotFoundException {

        // Create a new queue to hold tokens
        Queue<String> tokenStream = new LinkedList<>();

        // Create a new scanner object
        Scanner sc = new Scanner(new BufferedReader(new FileReader(inputFile)));

        // While file has values, add them to the queue
        while (sc.hasNext()) {

            tokenStream.add(sc.next());

        }

        // Return the new queue containing 'tokens'
        return tokenStream;
    }
}
