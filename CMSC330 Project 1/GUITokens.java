/**
 * This class defines the tokens found in our GUI, we also provide several methods that
 * check if the token is a number and whether it is a string
 */
public final class GUITokens {

    static final String COLON = ":";
    static final String SEMICOLON = ";";
    static final String PERIOD = ".";
    static final String WINDOW_KEYWORD = "Window";
    static final String FLOW_KEYWORD = "Flow";
    static final String BUTTON_KEYWORD = "Button";
    static final String LABEL_KEYWORD = "Label";
    static final String TEXTFIELD_KEYWORD = "Textfield";
    static final String LAYOUT_KEYWORD = "Layout";
    static final String GRID_KEYWORD = "Grid";
    static final String GROUP_KEYWORD = "Group";
    static final String PANEL_KEYWORD = "Panel";
    static final String RADIO_KEYWORD = "Radio";
    static final String L_PARENS = "(";
    static final String R_PARENS = ")";
    static final String COMMA = ",";

    static final String END = "End";


    static boolean isNumberToken(String s) {

        try {

            Integer.parseInt(s);
            return true;

        } catch (NumberFormatException e) {
            return false;
        }
    }


    static boolean isStringToken(String s) {
        if (s.startsWith("\"") && s.endsWith("\"")) {
            return true;
        } else
            return false;
    }

    static boolean isAWidget(String s) {
        if (s.equals(TEXTFIELD_KEYWORD) || s.equals(BUTTON_KEYWORD) || s.equals(LABEL_KEYWORD)
            || s.equals(RADIO_KEYWORD)) {
            return true;
        } else
            return false;
    }

}
