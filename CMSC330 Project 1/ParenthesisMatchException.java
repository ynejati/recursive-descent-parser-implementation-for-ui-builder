/**
 * Created by yousufnejati on 11/11/15.
 */
public class ParenthesisMatchException extends ParseError {


    public ParenthesisMatchException(String message) {
        super(message);
    }
}
