/**
 * Created by yousufnejati on 11/11/15.
 */
public class ParseError extends Exception {

    public ParseError(String message) {
        super(message);
    }
}
